#!/bin/sh
# Uploads files to s3 like service

. ./utils/log.sh
. ./utils/sys.sh

if ! check_bin_deps aws; then
    exit 1
fi

TYPE=${1}
REMOTE_LOCATION=${2}
DATA=${3}

shift 3

if [ -z "${AWS_ACCESS_KEY_ID}" ]; then
    export AWS_ACCESS_KEY_ID="${S3_ACCESS_KEY}"
fi
if [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    export AWS_SECRET_ACCESS_KEY="${S3_SECRET_KEY}"
fi
if [ -z "${AWS_DEFAULT_REGION}" ]; then
    export AWS_DEFAULT_REGION="${S3_REGION}"
fi

if [ "${TYPE}" = "file" ]; then
    aws s3 cp "${DATA}" "s3://${REMOTE_LOCATION}" "$@"
    res=${?}
elif [ "${TYPE}" = "dir" ]; then
    aws s3 sync "${DATA}" "s3://${REMOTE_LOCATION}" "$@"
    res=${?}
elif [ "${TYPE}" = "api" ]; then
    bucket=$(echo "${REMOTE_LOCATION}" | cut -d'/' -f1)
    key=$(echo "${REMOTE_LOCATION}" | sed "s/${bucket}\///g")
    aws s3api put-object --bucket "${bucket}" --key "${key}/$(basename "${DATA}")" --body "${DATA}" "$@"
    res=${?}
else
    log "Invalid upload type" error
    res=3
fi

exit ${res}
