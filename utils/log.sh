# Logging

error() {
    printf "\033[0;31m%s\033[m\n" "${1}"
}

info() {
    printf "\033[0;36m%s\033[m\n" "${1}"
}

warning() {
    printf "\033[1;33m%s\033[m\n" "${1}"
}

success() {
    printf "\033[0;32m%s\033[m\n" "${1}"
}

log() {         # Log messages
                # Arg $1 = message
                # Arg $2 = level
    message=${1}
    level=${2:-"info"}
    log_format="$(date +"%Y-%m-%d %H:%M:%S") $(echo "${level}" | awk '{print toupper($0)}') ${USER} ${TOOL}: "
    log_message=${log_format}${message}
    

    case ${level} in
        info) info "${log_message}" ;;
        warning) warning "${log_message}" ;;
        error) error "${log_message}" ;;
        success) success "${log_message}" ;;
        *) info "${log_message}" ;;
    esac

    if [ -d "${LOG_LOG_FILE}" ]; then
        lfile="${LOG_LOG_FILE}/${TOOL}.log"
    elif [ -f "${LOG_LOG_FILE}" ]; then
        lfile="${LOG_LOG_FILE}"
    fi

    if [ ! -w "${lfile}" ]; then
        unset lfile
    fi

    if [ -n "${lfile}" ] && [ "${TOOL}" != "run-saas-tool" ]; then
        echo "${log_message}" >> "${lfile}"
    fi
}

setup_log() {  # Setup logging
    # LOG_LOG_FILE not defined (user doesn't want log to file) so we skip the
    # entire function
    if [ -z "${LOG_LOG_FILE}" ] ; then
        return
    fi

    if [ ! -e "${LOG_LOG_FILE}" ]; then
        parent=$(dirname "${LOG_LOG_FILE}")
        if [ ! -d "${parent}" ]; then
            log "Unable to log to file. Log file parent (${parent}) does not exist" warning
        elif [ ! -w "${parent}" ]; then
            log "Unable to create log file. Parent (${parent}) is not writable" warning
        elif [ "$("${LOG_LOG_FILE}" | grep "\.log$" || "CONTROL")" != "CONTROL" ]; then
            touch "${LOG_LOG_FILE}"
            log "Created log file ${LOG_LOG_FILE}" success
        else
            mkdir "${LOG_LOG_FILE}"
            log "Created log directory ${LOG_LOG_FILE}" success
        fi
    elif [ ! -w "${LOG_LOG_FILE}" ]; then
        log "Unable to log to file. Log file/directory (${LOG_LOG_FILE}) is not writable" warning
    fi
}
