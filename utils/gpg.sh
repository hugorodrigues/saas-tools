# GPG Tools

. ./utils/log.sh
. ./utils/sys.sh

encrypt()		# Encrypt file
			    # Arg $1 = file to encrypt
    			# Arg $2 = keyid
{
    if [ -z "$1" ]; then
        log "Must provide a file to encrypt" error
        return 1
    fi
    compression_level=${2-6}
    GPG=gpg2
    if ! check_bin_deps ${GPG}; then
        check_bin_deps gpg
        GPG=gpg
        if ! check_bin_deps ${GPG}; then
            log "Unable to find gpg" error
            return 0
        fi
    fi
    delete_gpg_home=0
    if [ -z "${GNUPGHOME}" ]; then
        GNUPGHOME=$(mktemp -d)
        export GNUPGHOME
        delete_gpg_home=1
    fi
    if [ "${GPG_AUTO_IMPORT_KEYS}" -eq 1 ]; then
        for key in files/gpg_keys/*; do
            ${GPG} --import "${key}"
        done
    fi
    file=${1}
    recipient=${3:-${GPG_DEFAULT_RECIPIENT}}
    ${GPG} -z "${compression_level}" --encrypt --recipient "${recipient}" --trust-model always "${file}"
    if [ $delete_gpg_home -eq 1 ]; then
        rm -rf "${GNUPGHOME}"
        unset GNUPGHOME
    fi
    return 0
}

