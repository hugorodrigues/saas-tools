# System utils

. ./utils/log.sh

check_exec()    # Check if a executable is available
                # Arg $1 = executable
{
    if [ -z "${1}" ]; then
        return 1
    fi
    command -v "${1}" > /dev/null
    return ${?}
}

check_bin_deps()    # Check if all binary depencies are met
                    # Args = executables to check
{
    for execut in "$@"; do
        if ! check_exec "${execut}"; then
            log "Missing executable ${execut}" error
            return 1
        fi
    done
    return 0
}

compress_file()    # If available, compress files
                   # Arg $1 = file to compress
                   # Return: new file name
{
    compress "${1}"
    if [ ! -e "${1}.Z" ]; then
        echo "${1}"
    else
        echo "${1}.Z"
    fi
}


compress_files_in_dir()    # If available, compress files inside directory
                           # Arg $1 = Directory where the files are
{
    compress -r "${1}"
}

get_date()    # Get date using the DATE_FORMAT setting
{
    date +"${DATE_FORMAT}"
}

random_str()    # Get a random string
{
   tr -dc 'a-zA-Z0-9' < /dev/urandom  | fold -w 32 | head -n 1
}
