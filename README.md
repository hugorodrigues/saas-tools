# saas-tools
A collection of scripts that I use on my servers.
This tools are desined for Alpine Linux (busybox) and may need some tunning for other distros


Available Tools
----------------
tool | summary
--- | --- 
[upload](upload/) | Uploads files and directories
[update](update/) | Update this set of tools (if downloaded with git)

Configuration
----------------
It's possible to configure some settings by creating a file caled config.env under the root of this package
There's a file called config.env.example with the available configurations
